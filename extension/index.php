<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

include_once dirname(__FILE__) . '/includes/bootstrap.php';

$showRightColumn = $this->countModules('right-sidebar') > 0 || $this->countModules('position-0') > 0 || $this->countModules('position-7') > 0;
$showLeftColumn  = $this->countModules('left-sidebar') > 0 || $this->countModules('position-8') > 0;
$showSingleSidebar = (true === $showRightColumn && false === $showLeftColumn) || (false === $showRightColumn && true === $showLeftColumn);
$showNoColumns = false === $showRightColumn && false === $showLeftColumn;
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
  <?php include_once dirname(__FILE__) . '/includes/head.php'; ?>
	<body <?php if ($itemId): ?> id="item<?php echo $itemId; ?>" <?php endif; ?> class="bg-white font-source-sans antialiased font-normal text-black leading-normal <?=$bodyclass?>">
    <?php if ($this->countModules('toolbar')): ?>
      <div id="toolbar" class="py-6">
        <div class="wrapper">
          <div class="lg:flex">
            <jdoc:include type="modules" name="toolbar" style="tabata" class="lg:flex-auto" header-class="mb-3 lg:mb-2 text-grey-dark uppercase tracking-wide font-bold text-sm"/>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?php if ($this->countModules('position-1') || $this->countModules('top-nav')): ?>
      <div id="top-nav" class="bg-green-darker border-b border-grey-dark py-6">
        <div class="wrapper">
          <jdoc:include type="modules" name="top-nav" style="tabata" />
          <jdoc:include type="modules" name="position-1" style="tabata" />
        </div>
      </div>
    <?php endif; ?>
    <?php if ($this->countModules('top')): ?>
      <div id="top" class="p-6 lg:flex">
        <jdoc:include type="modules" name="top" style="tabata" class="lg:flex-auto" header-class="mb-3 lg:mb-2 text-grey-dark uppercase tracking-wide font-bold text-sm"/>
      </div>
    <?php endif; ?>
    <?php if ($this->countModules('top-content')): ?>
      <div id="top-content" class="py-6">
        <div class="wrapper">
          <div class="flex">
            <jdoc:include type="modules" name="top-content" style="tabata" class="flex-auto" header-class="mb-3 lg:mb-2 text-grey-dark uppercase tracking-wide font-bold text-sm"/>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <div class="wrapper">
      <div class="lg:flex">
        <?php if ($showLeftColumn): ?>
          <aside id="sidebar" class="hidden z-90 top-16 bg-grey-darker w-full border-b lg:-mb-0 lg:static lg:border-b-0 lg:pt-0 lg:w-1/4 lg:block lg:border-0 xl:w-1/5">
            <div class="lg:block lg:relative lg:sticky lg:top-16">
              <div id="left-sidebar" class="px-6 pt-6 overflow-y-auto text-base lg:text-sm  sticky?lg:h-(screen-16)">
                <jdoc:include type="modules" name="position-8" style="tabata" class="mb-6" header-class="mb-3 lg:mb-2 text-green-light uppercase tracking-wide font-bold text-sm"/>
                <jdoc:include type="modules" name="left-sidebar" style="tabata" class="mb-6" header-class="mb-3 lg:mb-2 text-green-light uppercase tracking-wide font-bold text-sm"/>
              </div>
            </div>
          </aside>
        <?php endif; ?>
        <?php if ($showNoColumns) : ?>
          <div id="content-wrapper" class="min-h-screen w-full lg:static lg:max-h-full lg:overflow-visible">
        <?php elseif ($showSingleSidebar) : ?>
          <div id="content-wrapper" class="min-h-screen w-full lg:static lg:max-h-full lg:overflow-visible lg:w-3/4 xl:w-4/5">
        <?php else: ?>
          <div id="content-wrapper" class="min-h-screen w-full lg:static lg:max-h-full lg:overflow-visible lg:w-2/4 xl:w-3/5">
        <?php endif; ?>
          <div id="content" class="content">
            <?php if ($this->countModules('content-top')): ?>
                <div id="content-top" class="lg:flex py-6">
                  <jdoc:include type="modules" name="content-top" style="tabata" class="flex-auto" header-class="mb-3 lg:mb-2 text-grey-dark uppercase tracking-wide font-bold text-sm" />
                </div>
            <?php endif; ?>
            <div class="md:pt-12 md:px-6 md:pb-8 lg:pt-28 w-full">
              <?php if ($this->countModules('pre-content')): ?>
                <div id="pre-content" class="lg:flex py-6">
                  <jdoc:include type="modules" name="pre-content" style="tabata" class="flex-auto" header-class="mb-3 lg:mb-2 text-grey-dark uppercase tracking-wide font-bold text-sm" />
                </div>
              <?php endif; ?>
              <?php if ($this->countModules('position-3')): ?>
                <div id="position-3" class="lg:flex">
                  <jdoc:include type="modules" name="position-3" style="tabata" class="flex-auto" header-class="mb-3 lg:mb-2 text-grey-dark uppercase tracking-wide font-bold text-sm" />
                </div>
              <?php endif; ?>
              <jdoc:include type="message" />
              <jdoc:include type="component" />
              <?php if ($this->countModules('post-content')): ?>
                <div id="post-content" class="lg:flex py-6">
                  <jdoc:include type="modules" name="post-content" style="tabata" class="flex-auto" header-class="mb-3 lg:mb-2 text-grey-dark uppercase tracking-wide font-bold text-sm" />
                </div>
              <?php endif; ?>
              <?php if ($this->countModules('position-2')): ?>
                <div id="position-2" class="lg:flex">
                  <jdoc:include type="modules" name="position-2" style="tabata" class="flex-auto" header-class="mb-3 lg:mb-2 text-grey-dark uppercase tracking-wide font-bold text-sm" />
                </div>
              <?php endif; ?>
            </div>
            <?php if ($this->countModules('content-bottom')): ?>
              <div id="content-bottom" class="lg:flex py-6">
                <jdoc:include type="modules" name="content-bottom" style="tabata" class="flex-auto" header-class="mb-3 lg:mb-2 text-grey-dark uppercase tracking-wide font-bold text-sm" />
              </div>
            <?php endif; ?>
          </div>
        </div>
        <?php if ($showRightColumn) : ?>
            <aside id="right" class="z-90 top-16 w-full border-b lg:-mb-0 lg:static bg-grey-darker lg:border-b-0 lg:pt-0 lg:w-1/4 lg:block lg:border-0 xl:w-1/5">
              <div class="lg:block lg:relative lg:sticky lg:top-16">
                <div class="px-6 pt-6 overflow-y-auto text-base lg:text-sm lg:pl-6 lg:pr-8 sticky?lg:h-(screen-16)">
                  <?php if ($this->countModules('right-sidebar')): ?>
                    <div id="right-sidebar">
                      <jdoc:include type="modules" name="right-sidebar" style="tabata" class="mb-6" header-class="mb-3 lg:mb-2 text-green-light uppercase tracking-wide font-bold text-sm"/>
                    </div>
                  <?php endif; ?>
                  <?php if ($this->countModules('position-0')): ?>
                    <div id="position-0">
                      <jdoc:include type="modules" name="position-0" style="tabata" class="mb-6" header-class="mb-3 lg:mb-2 text-green-light uppercase tracking-wide font-bold text-sm"/>
                    </div>
                  <?php endif; ?>
                  <?php if ($this->countModules('position-7')): ?>
                    <div id="position-7">
                      <jdoc:include type="modules" name="position-7" style="tabata" class="mb-6" header-class="mb-3 lg:mb-2 text-green-light uppercase tracking-wide font-bold text-sm"/>
                    </div>
                  <?php endif; ?>
                </div>
              </div>
            </aside><!-- end right -->
        <?php endif ?>
      </div>
    </div>
    <?php if ($this->countModules('bottom-content')): ?>
      <div id="bottom-content">
        <div class="wrapper">
          <div class="lg:flex py-6">
            <jdoc:include type="modules" name="bottom-content" style="tabata" class="lg:flex-auto" header-class="mb-3 lg:mb-2 text-grey-dark uppercase tracking-wide font-bold text-sm"/>
          </div>
        </div>
      </div>
    <?php endif; ?>
    <?php if ($this->countModules('bottom')): ?>
      <div id="bottom p-6">
        <div class="lg:flex">
          <jdoc:include type="modules" name="bottom" style="tabata" class="lg:flex-auto" header-class="mb-3 lg:mb-2 text-grey-dark uppercase tracking-wide font-bold text-sm"/>
        </div>
      </div>
    <?php endif; ?>
      <div id="footer" class="bg-grey-darker text-grey border-t border-grey-dark  py-6">
        <div class="wrapper">
          <button type="button" id="back-to-top" class="float-right">Back to top</button>
          <p>&copy; <?php echo date('Y'); ?> <?php echo $sitename; ?></p>
          <?php if ($this->countModules('footer')): ?>
            <div class="lg:flex">
              <jdoc:include type="modules" name="footer" style="tabata" class="lg:flex-auto" header-class="mb-3 lg:mb-2 text-green-light uppercase tracking-wide font-bold text-sm"/>
            </div>
          <?php endif; ?>
        </div>
      </div>
		<jdoc:include type="modules" name="debug" style="none" />
	</body>
</html>
