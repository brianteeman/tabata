<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

include_once dirname(__FILE__) . '/includes/bootstrap.php';

$showRightColumn = true;
$showLeftColumn  = true;
$showSingleSidebar = false;
$showNoColumns = false;
?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>">
  <?php include_once dirname(__FILE__) . '/includes/head.php'; ?>
	<body <?php if ($itemId): ?> id="item<?php echo $itemId; ?>" <?php endif; ?> class="bg-white font-source-sans antialiased font-normal text-black leading-normal <?=$bodyclass?>">
    <div id="toolbar" class="bg-grey-darker text-white py-6">
      <div class="wrapper">
        <p>Position: toolbar</p>
      </div>
    </div>

    <div id="top-nav" class="bg-green-darker border-b border-grey-dark py-6 text-white">
      <div class="wrapper">
        <p>Position: top-nav</p>
      </div>
    </div>

    <div id="top" class="bg-blue border-b border-grey-dark p-6 text-white">
      <p>Position: top</p>
    </div>

    <div id="top-content">
      <div class="wrapper">
        <div class="bg-grey border-b border-grey-dark p-6 text-white">
          <p>Position: top-content</p>
        </div>
      </div>
    </div>

    <div class="wrapper">
      <div class="lg:flex">
          <aside id="sidebar" class="hidden z-90 top-16 bg-grey-darker w-full border-b lg:-mb-0 lg:static lg:border-b-0 lg:pt-0 lg:w-1/4 lg:block lg:border-0 xl:w-1/5">
            <div class="lg:block lg:relative lg:sticky lg:top-16">
              <div id="left-sidebar" class="px-6 pt-6 overflow-y-auto text-base text-white lg:text-sm sticky?lg:h-(screen-16)">
                <p>Position: left-sidebar</p>
              </div>
            </div>
          </aside>
          <div id="content-wrapper" class="min-h-screen w-full lg:static lg:max-h-full lg:overflow-visible lg:w-2/4 xl:w-3/5">
          <div id="content" class="content">
            <div id="content-top" class="lg:flex bg-red p-6 text-white">
              <p>Position: content-top</p>
            </div>
            <div class="md:pt-12 md:px-6 md:pb-8 lg:pt-28 w-full">
              <div id="pre-content" class="lg:flex bg-green-darker p-6 text-white">
                <p>Position: pre-content</p>
              </div>
              <div id="system-message" class="bg-grey text-white p-6">
                <p>System messages</p>
              </div>
              <div id="component" class="bg-blue text-white p-6 h-screen">
                <p>Component</p>
              </div>
              <div id="post-content" class="lg:flex bg-green-darker text-white p-6">
                <p>Position: post-content</p>
              </div>
            </div>
            <div id="content-bottom" class="lg:flex bg-red p-6 text-white">
              <p>Position: content-bottom</p>
            </div>
          </div>
        </div>
        <?php if ($showRightColumn) : ?>
            <aside id="right" class="z-90 top-16 w-full border-b lg:-mb-0 lg:static bg-grey-darker lg:border-b-0 lg:pt-0 lg:w-1/4 lg:block lg:border-0 xl:w-1/5">
              <div class="lg:block lg:relative lg:sticky lg:top-16">
                <div class="px-6 pt-6 overflow-y-auto text-base lg:text-sm lg:pl-6 lg:pr-8 sticky?lg:h-(screen-16)">
                    <div id="right-sidebar" class="text-white">
                      <p>Position: right-sidebar</p>
                    </div>
                </div>
              </div>
            </aside><!-- end right -->
        <?php endif ?>
      </div>
    </div>
    <div id="bottom-content">
      <div class="wrapper">
        <div class="bg-grey border-b border-grey-dark p-6 text-white">
          <p>Position: bottom-content</p>
        </div>
      </div>
    </div>
    <div id="bottom" class="bg-blue text-white p-6">
      <p>Position: bottom</p>
    </div>
      <div id="footer" class="bg-grey-darker text-grey border-t border-grey-dark  py-6">
        <div class="wrapper">
          <button type="button" id="back-to-top" class="float-right">Back to top</button>
          <p>&copy; <?php echo date('Y'); ?> <?php echo $sitename; ?></p>
          <div class="lg:flex bg-grey text-white p-6">
            <p>Position: footer</p>
          </div>
        </div>
      </div>
		<jdoc:include type="modules" name="debug" style="none" />
	</body>
</html>