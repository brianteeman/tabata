<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

function modChrome_tabata($module, &$params, &$attribs)
{
	$moduleTag = htmlspecialchars($params->get('module_tag', 'div'), ENT_QUOTES, 'UTF-8');
	$headerTag = htmlspecialchars($params->get('header_tag', 'h3'), ENT_QUOTES, 'UTF-8');

	$moduleClass = tabataGuessModuleClass($module, $params, $attribs);
	$headerClass = tabataGuessModuleHeaderClass($module, $params, $attribs);

	if (!empty ($module->content)) : ?>
		<<?=$moduleTag?> class="<?=trim($moduleClass)?>">
			<?php if ((bool) $module->showtitle) : ?>
				<<?=$headerTag?> class="<?=trim($headerClass)?>"><?php echo $module->title; ?></<?=$headerTag?>>
			<?php endif; ?>
			<?php echo $module->content; ?>
		</<?=$moduleTag?>>
	<?php endif;
}

function tabataGuessModuleClass($module, $params, $attribs)
{
	$class = 'module ' . str_replace('_', '-', $module->module);

	if (!empty($module->id))
	{
		$class .= ' ' . $class . '-' . $module->id;
	}

	$jdocClass = isset($attribs['class']) ? $attribs['class'] : '';

	$suffix = trim($params->get('moduleclass_sfx'));

	// Empty suffix: Use jdoc attrib
	if (empty($suffix))
	{
		return trim($class . ' ' . $jdocClass);
	}

	// Suffix starting with _ or & or +: use jdoc attrib + Suffix
	if (in_array($suffix[0], ['_', '&', '+'], true))
	{
		return trim($class . ' ' . $jdocClass . ' ' . htmlspecialchars(substr($suffix, 1), ENT_COMPAT, 'UTF-8'));
	}

	return trim($class . ' ' . htmlspecialchars($suffix, ENT_COMPAT, 'UTF-8'));
}

function tabataGuessModuleHeaderClass($module, $params, $attribs)
{
	$class = 'module-header';

	$jdocClass = isset($attribs['header-class']) ? $attribs['header-class'] : '';

	$paramsClass = trim($params->get('header_class'));

	// Empty param: Use jdoc attrib
	if (empty($paramsClass))
	{
		return trim($class . ' ' . $jdocClass);
	}

	// Param starting with _ or & or +: use jdoc attrib + module param
	if (in_array($paramsClass[0], ['_', '&', '+'], true))
	{
		return trim($class . ' ' . $jdocClass . ' ' . htmlspecialchars(substr($paramsClass, 1), ENT_COMPAT, 'UTF-8'));
	}

	return trim($class . ' ' . htmlspecialchars($paramsClass, ENT_COMPAT, 'UTF-8'));
}
