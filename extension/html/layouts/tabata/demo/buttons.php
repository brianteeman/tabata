<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="tabata-demo-buttons mb-6">
	<h3 class="text-3xl">Buttons</h3>
	<button class="button mb-2">button</button>
	<button class="button-dark mb-2">button-dark</button>
	<button class="button-primary mb-2">button-primary</button>
	<button class="button-primary-light mb-2">button-primary-light</button>
	<button class="button-alert mb-2">button-alert</button>
	<button class="button-info mb-2">button-info</button>
	<button class="button-warning mb-2">button-warning</button>
</div>
<div class="tabata-demo-outline-buttons mb-6">
	<h3 class="text-3xl">Outline buttons</h3>
	<button class="button-outline mb-2">button-outline</button>
	<button class="button-outline-dark mb-2">button-outline</button>
	<button class="button-outline-primary mb-2">button-outline-primary</button>
	<button class="button-outline-primary-light mb-2">button-outline-primary-light</button>
	<button class="button-outline-alert mb-2">button-outline-primary-light</button>
	<button class="button-outline-info mb-2">button-outline-primary-info</button>
	<button class="button-outline-warning mb-2">button-outline-primary-warning</button>
</div>
