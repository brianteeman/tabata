<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\CMS\Router\Route;
use Joomla\Registry\Registry;

JLoader::register('ContentHelperRoute', JPATH_SITE . '/components/com_content/helpers/route.php');

/**
 * Layout variables
 *
 * @var  stdClass   $category  Category as it comes from joomla models/helpers
 * @var  Registry   $params    Parameters
 */
extract($displayData);

$link = Route::_(ContentHelperRoute::getCategoryRoute($category->id, $category->language));
?>
<div class="w-full mb-6 category-preview">
	<div class="border rounded overflow-hidden border-grey-light bg-white flex flex-col justify-between leading-normal">
		<div class="p-4">
			<div class="flex items-center py-2 text-sm">
				<div class="flex-auto text-black font-bold text-xl py-2 xs:float-left lg:float-none">
					<a class="text-green-darker hover:text-green-dark" href="<?php echo $link ?>" itemprop="url">
						<?php echo $this->escape($category->title); ?>
					</a>
				</div>
				<?php if ($params->get('show_cat_num_articles_cat') == 1) :?>
					<p class="text-grey-dark mr-2">
						<?php echo JText::_('COM_CONTENT_NUM_ITEMS'); ?>
					</p>
					<p class="text-black leading-none">
						<?php echo $category->numitems; ?>
					</p>
				<?php endif; ?>
			</div>
			<?php if ($params->get('show_description_image') && $category->getParams()->get('image')) : ?>
				<img src="<?php echo $category->getParams()->get('image'); ?>" alt="<?php echo htmlspecialchars($category->getParams()->get('image_alt'), ENT_COMPAT, 'UTF-8'); ?>" />
			<?php endif; ?>
			<?php if ($params->get('show_subcat_desc_cat') == 1) : ?>
				<?php if ($category->description) : ?>
					<div class="category-desc">
						<?php echo JHtml::_('content.prepare', $category->description, '', 'com_content.categories'); ?>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	</div>
</div>
