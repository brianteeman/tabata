<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_breadcrumbs
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Get rid of duplicated entries on trail including home page when using multilanguage
for ($i = 0; $i < $count; $i++)
{
	if (empty($list[$i]->name))
	{
		unset($list[$i]);
	}
	elseif ($i === 1 && !empty($list[$i]->link) && !empty($list[$i - 1]->link) && $list[$i]->link === $list[$i - 1]->link)
	{
		unset($list[$i]);
	}
}

// Avoid showing single page links
if (count($list) < 2 )
{
	return;
}

// Find last and penultimate items in breadcrumbs list
end($list);
$last_item_key   = key($list);
prev($list);
$penult_item_key = key($list);

// Make a link if not the last item in the breadcrumbs
$show_last = $params->get('showLast', 1);
?>
<div aria-label="<?php echo $module->name; ?>" role="navigation" class="py-6">
	<ul itemscope itemtype="https://schema.org/BreadcrumbList" class="flex breadcrumb<?php echo $moduleclass_sfx; ?>">
		<?php foreach ($list as $key => $item) : ?>
			<?php if (!empty($item->link)) : ?>
				<li class="mr-4" itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
					<?php if (!empty($item->link)) : ?>
						<a itemprop="item" href="<?php echo $item->link; ?>" class="link-primary pathway"><span itemprop="name"><?php echo $item->name; ?></span></a>
					<?php else : ?>
						<span itemprop="item">
							<span itemprop="name">
								<?php echo $item->name; ?>
							</span>
						</span>
					<?php endif; ?>

					<meta itemprop="position" content="<?php echo $key + 1; ?>">
				</li>
			<?php else: ?>
				<li class="mr-4">
					<span class="text-grey-dark">
						<?php echo $item->name; ?>
					</span>
				</li>
			<?php endif ?>
		<?php endforeach; ?>
	</ul>
</div>
