<?php
/**
 * @package     Bubu.Template
 * @subpackage  Tabata
 *
 * @copyright   Copyright (C) 2019 bubutechnologies.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<head>
	<meta charset="<?php echo $this->_charset; ?>">

	<!-- include joomla generated head  -->
	<jdoc:include type="head" />

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

    <link rel="apple-touch-icon" sizes="57x57" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/favicon-16x16.png">
    <link rel="manifest" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/img/icons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/css/template<?php if (!$isDevelopmentModeEnabled) : ?>.min<?php endif?>.css">
    <script src="<?=$this->baseurl?>/templates/<?=$this->template?>/assets/js/template<?php if (!$isDevelopmentModeEnabled) : ?>.min<?php endif?>.js" defer></script>

    <?php include_once __DIR__ . '/google-analytics.php'; ?>
</head>
