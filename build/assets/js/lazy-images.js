(function() {
  'use strict';

  var swapAttr = function (img) {
    var src = img.getAttribute('data-src');
    var srcset = img.getAttribute('data-srcset');

    img.src = src;
    if (srcset) img.setAttribute('srcset', srcset);
  };

  var onPolyfillReady = function () {
    var images = [].slice.call(document.querySelectorAll('img[loading="lazy"]'));
    if (!images.length) return;
    images.forEach(lazyload);
  };

  var lazyload = function (element) {
    var io = new IntersectionObserver(function (entries, observer) {
      entries.forEach(function (entry) {
        if (entry.isIntersecting) {
          swapAttr(entry.target)
          observer.disconnect();
        }
      });
    });

    io.observe(element)
  };


  function init() {
    // Let GC remove unneeded parts
    document.removeEventListener('DOMContentLoaded', init);

    initImagesLazyLoad();
  }

  var initImagesLazyLoad = function () {
    var images = [].slice.call(document.querySelectorAll('img[loading="lazy"]'));
    if (!images.length) return;
    if ('loading' in HTMLImageElement.prototype) {
      images.forEach(swapAttr);
    } else {
      if (!'IntersectionObserver' in window &&
        !'IntersectionObserverEntry' in window &&
        !'intersectionRatio' in window.IntersectionObserverEntry.prototype) {
        // load polyfill now
        document.addEventListener('intersection-observer-loaded', onPolyfillReady)
        var script = document.createElement('script');
        script.src = '/templates/tabata/assets/js/io-polyfill.js';
        document.head.appendChild(script);
      } else {
        images.forEach(lazyload);
      }
    }
  };

  document.addEventListener("DOMContentLoaded", init);
})();
