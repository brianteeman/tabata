const gulp = require('gulp');

const browserSync = require('browser-sync').create();
const cleanCSS    = require('gulp-clean-css');
const concat      = require('gulp-concat');
const del         = require('del');
const fs          = require('fs');
const gutil       = require('gulp-util');
const plumber     = require('gulp-plumber');
const rename      = require('gulp-rename');
const postcss     = require('gulp-postcss');
const purgecss    = require('gulp-purgecss');
const tailwindcss = require('tailwindcss');
const zip         = require('gulp-zip');
const xml2js      = require('xml2js');
const parser      = new xml2js.Parser();
const uglify      = require('gulp-uglify');

const extension = require('./package.json');
const config  = require('./gulp-config.json');

const defaultBrowserConfig = {
	proxy : "localhost"
}

const browserConfig = config.hasOwnProperty('browserConfig') ? config.browserConfig : defaultBrowserConfig;

const tplName   = "tabata";
const tplBase   = "site";

const extPath   = '../extension';
const assetsPath = './assets';
const wwwPath = config.wwwDir + '/templates/' + tplName;

function initBrowserSync() {
	return browserSync.init(browserConfig)
}

function reloadBrowserSync(cb) {
	browserSync.reload({stream:true});
	cb();
}

function clean() {
	return del(wwwPath, {force : true});
}

function copy() {
	return gulp.src(extPath + '/**',{ base: extPath })
		.pipe(gulp.dest(wwwPath))
		.pipe(browserSync.stream());
}

function compileJavascriptFiles(src, ouputFileName, destinationFolder) {
	return gulp.src(src)
		.pipe(plumber({ errorHandler: onError }))
		.pipe(concat(ouputFileName))
		.pipe(gulp.dest(extPath + '/' + destinationFolder))
		.pipe(gulp.dest(wwwPath + '/' + destinationFolder))
		.pipe(browserSync.stream())
		.pipe(uglify())
		.pipe(rename(function (path) {
			path.basename += '.min';
		}))
		.pipe(gulp.dest(extPath + '/' + destinationFolder))
		.pipe(gulp.dest(wwwPath + '/' + destinationFolder))
		.pipe(browserSync.stream());
}

class TailwindExtractor {
  static extract(content) {
    return content.match(/[A-Za-z0-9-_:\/]+/g) || [];
  }
}

function css(src, destinationFolder, options)
{
	return gulp.src(assetsPath + '/css/template.css')
		.pipe(plumber({ errorHandler: onError }))
		.pipe(postcss([
			require('postcss-import'),
			require('precss'),
			tailwindcss('./tailwind.config.js'),
			require('autoprefixer'),
		]))
		.pipe(gulp.dest(extPath + '/assets/css'))
		.pipe(gulp.dest(wwwPath + '/assets/css'))		
		.pipe(browserSync.stream());
}

function js() {
	return gulp.src([
			assetsPath + '/js/go-top.js',
			assetsPath + '/js/lazy-images.js',
			assetsPath + '/js/navbar-burguers.js'
		])
		.pipe(plumber({ errorHandler: onError }))
		.pipe(concat('template.js'))
		.pipe(uglify())
		.pipe(gulp.dest(extPath + '/assets/js'))
		.pipe(gulp.dest(wwwPath + '/assets/js'))
		.pipe(browserSync.stream());
}

function onError(err) {
	return gutil.log(gutil.colors.green(err));
};

function distCSS() {
	return gulp.src(assetsPath + '/css/template.css')
		.pipe(postcss([
		  require('postcss-import'),
		  require('precss'),
		  tailwindcss('./tailwind.config.js'),
		  require('autoprefixer'),
		]))
		.pipe(
		  purgecss({
			content: [
				extPath + '/html/**/*.php',
				extPath + '/html/*.html',
				extPath + '/*.php'
			],
	        extractors: [
	          {
	            extractor: TailwindExtractor,
	            extensions: ["php"]
	          }
	        ]
		  })
		)		
		.pipe(cleanCSS())
		.pipe(rename('template.min.css'))
		.pipe(gulp.dest(extPath + '/assets/css'))
		.pipe(gulp.dest(wwwPath + '/assets/css'));
}

function distJS() {
	return gulp.src([
			assetsPath + '/js/go-top.js',
			assetsPath + '/js/lazy-images.js',
			assetsPath + '/js/navbar-burguers.js'
		])
		.pipe(plumber({ errorHandler: onError }))
		.pipe(concat('template.js'))
		.pipe(uglify())
		.pipe(rename(function (path) {
			path.basename += '.min';
		}))
		.pipe(gulp.dest(extPath + '/assets/js'))
		.pipe(gulp.dest(wwwPath + '/assets/js'));
}

function release(cb) {
	return fs.readFile(extPath + '/templateDetails.xml', function(err, data) {
		parser.parseString(data, function (err, result) {
			var version = result.extension.version[0];

			var fileName = extension.name + '-v' + version + '.zip';

			return gulp.src(extPath + '/**',{ base: extPath })
				.pipe(zip(fileName))
				.pipe(gulp.dest('releases'))
				.on('end', cb);
		});
	});
}

function watch(cb) {
	gulp.watch(
		[
			extPath + '/html/*.html',
			extPath + '/html/**/*.php',
			extPath + '/includes/*.php',
			extPath + '/language/**/*.ini',
			extPath + '/*.php',
			extPath + '/templateDetails.xml'
		], 
		gulp.series(clean, copy, reloadBrowserSync)
	);
	gulp.watch('./tailwind.config.js', gulp.series(css, reloadBrowserSync));
	gulp.watch(assetsPath + '/js/**/*.js', gulp.series(js, reloadBrowserSync));
	gulp.watch(assetsPath + '/css/**/*.css', gulp.series(css, reloadBrowserSync));
	cb();
}

exports.clean = clean;
exports.copy = copy;
exports.css = css;
exports.dist = gulp.series(distCSS, distJS);
exports.distCSS = distCSS;
exports.distJS = distJS;
exports.js = js;
exports.release = release;
exports.watch = watch;
exports.default = gulp.series(
	clean, copy, 
	gulp.parallel(watch, initBrowserSync)
);
